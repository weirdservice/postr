#!/usr/bin/env node
import program from 'commander'
import NRP from 'node-redis-pubsub'
import crypto from 'crypto'
import mongoose from 'mongoose'

import pkg from '../package.json'

program
  .version(pkg.version)
  .usage('<configurations..>')
  .parse(process.argv)

const config = { port: 6379, scope: 'test' },
      nrp = new NRP(config)

const key = crypto.randomBytes(32).toString('hex')

// db
mongoose.connect('mongodb://localhost/test')
var db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected')
})

const feedSchema = mongoose.Schema({
    feed: String,
    link: String,
    hash: String,
    title: String,
    description: String
})

const Feed = mongoose.model('Feed', feedSchema)

var mock = {}
var requested = []

const exists = (data) => {
  Feed.findOne({ link: data.link }, (err, feed) => {
    if(err) return console.log(err)
    if(feed && feed.hash == data.hash) return;

    requested[data.feed] = requested[data.feed] || 0
    if(requested[data.feed] > 0) return;

    nrp.emit('scrapr:scrap', {url: data.feed})
    requested[data.feed]++
  })
  return true
}

const insert = (data) => {
  Feed.findOne({ link: data.link }, (err, feed) => {
    if(err) return console.log(err)
    if(feed && feed.hash == data.hash) return;

    var newFeed = new Feed(data)
    newFeed.save( (err, feed) => {
      if (err) return console.error(err)
      console.log('inserted', data.link, feed.hash)
    })
  })

  requested[data.feed] = requested[data.feed] || 0
  if (requested[data.feed] > 0) {
    requested[data.feed]--
  } else {
    delete requested[data.feed]
  }
  return true
}

const register = () => {
  nrp.emit('castr:add:postr', {id: key})
}

nrp.on('postr:exists:' + key, exists)
nrp.on('postr:insert:' + key, insert)

nrp.on('castr:callback', register)

process.on('SIGINT', () => {
    console.log("\nBye!")
    nrp.emit('castr:remove:postr', {id: key})
    nrp.quit()
    process.exit()
})

register()
console.log('Listen postr:* as ' + key)
