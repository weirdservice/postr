#!/usr/bin/env node
'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _nodeRedisPubsub = require('node-redis-pubsub');

var _nodeRedisPubsub2 = _interopRequireDefault(_nodeRedisPubsub);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _packageJson = require('../package.json');

var _packageJson2 = _interopRequireDefault(_packageJson);

_commander2['default'].version(_packageJson2['default'].version).usage('<configurations..>').parse(process.argv);

var config = { port: 6379, scope: 'test' },
    nrp = new _nodeRedisPubsub2['default'](config);

var key = _crypto2['default'].randomBytes(32).toString('hex');

// db
_mongoose2['default'].connect('mongodb://localhost/test');
var db = _mongoose2['default'].connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  // do some pubsub
  console.log('DB Connected');
});

var feedSchema = _mongoose2['default'].Schema({
  feed: String,
  link: String,
  hash: String,
  title: String,
  description: String
});

var Feed = _mongoose2['default'].model('Feed', feedSchema);

var mock = {};
var requested = [];

var exists = function exists(data) {
  Feed.findOne({ link: data.link }, function (err, feed) {
    if (err) return console.log(err);
    if (feed && feed.hash == data.hash) return;

    requested[data.feed] = requested[data.feed] || 0;
    if (requested[data.feed] > 0) return;

    nrp.emit('scrapr:scrap', { url: data.feed });
    requested[data.feed]++;
  });
  return true;
};

var insert = function insert(data) {
  Feed.findOne({ link: data.link }, function (err, feed) {
    if (err) return console.log(err);
    if (feed && feed.hash == data.hash) return;

    var newFeed = new Feed(data);
    newFeed.save(function (err, feed) {
      if (err) return console.error(err);
      console.log('inserted', data.link, feed.hash);
    });
  });

  requested[data.feed] = requested[data.feed] || 0;
  if (requested[data.feed] > 0) {
    requested[data.feed]--;
  } else {
    delete requested[data.feed];
  }
  return true;
};

var register = function register() {
  nrp.emit('castr:add:postr', { id: key });
};

nrp.on('postr:exists:' + key, exists);
nrp.on('postr:insert:' + key, insert);

nrp.on('castr:callback', register);

process.on('SIGINT', function () {
  console.log("\nBye!");
  nrp.emit('castr:remove:postr', { id: key });
  nrp.quit();
  process.exit();
});

register();
console.log('Listen postr:* as ' + key);
